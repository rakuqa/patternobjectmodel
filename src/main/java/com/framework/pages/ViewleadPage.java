package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewleadPage extends ProjectMethods {

	public ViewleadPage() {
		// apply PageFactory
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID,using="viewLead_firstName_sp") WebElement eleUsername;
	public ViewleadPage verifyFirstName(String data) {
		boolean verifiedExactText = verifyExactText(eleUsername, "Raku");
		System.out.println(verifiedExactText);
		return this;
	}
}
