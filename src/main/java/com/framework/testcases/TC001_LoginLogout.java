package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods {

	@BeforeTest
	public void setdata() {
		author ="Raku Raman KJ";
		category="SmokeTest";
		testCaseName="TC001_LoginLogout";
		testDescription="Login into Testleaf and create a Lead";
		testNodes="Leads";
		dataSheetName="TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username,String Password,String companyName,String firstName, String LastName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(Password)
		.clickLogin()
		.clickcrmsfa()
		.clickLead()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(LastName)
		.clickCreateLead();
	}

}