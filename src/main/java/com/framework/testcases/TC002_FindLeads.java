package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_FindLeads extends ProjectMethods {

	@BeforeTest
	public void setdata() {
		author = "Raku Raman KJ";
		category = "SmokeTest";
		testCaseName = "TC002_findaLead";
		testDescription = "Login into Testleaf and find a lead";
		testNodes = "Leads";
		dataSheetName = "TC002";
	}

	@Test(dataProvider = "fetchData")
	public void login(String username, String Password,String firstName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(Password)
		.clickLogin()
		.clickcrmsfa()
		.clickLead()
		.clickFindLead();
		/*.enterFirstname(firstName)
		.clickFindLeadButton();*/
	}

}
