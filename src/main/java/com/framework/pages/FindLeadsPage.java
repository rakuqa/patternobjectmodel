package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {

	public FindLeadsPage() {
		// apply PageFactory
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//input[@name='firstName'])[1]")
	WebElement eleFirstname;
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")
	WebElement elefindleadButton;
	
	public FindLeadsPage enterFirstname(String data) {
		// WebElement elePassword = locateElement("id", "password");
		clearAndType(eleFirstname, data);
		return this;
	}

	public FindLeadsPage clickFindLeadButton() {
		click(elefindleadButton);
		return this;
	}
	
}
