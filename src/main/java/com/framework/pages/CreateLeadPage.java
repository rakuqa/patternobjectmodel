package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "createLeadForm_companyName")	WebElement eleCompanyname;

	public CreateLeadPage enterCompanyName(String data) {
		// WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleCompanyname, data);
		return this;

	}
	@FindBy(how = How.ID, using = "createLeadForm_firstName") WebElement eleFirstName;

	public CreateLeadPage enterFirstName(String data) {
		// WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleFirstName, data);
		return this;

	}
	@FindBy(how = How.ID, using = "createLeadForm_lastName") WebElement eleLastName;

	public CreateLeadPage enterLastName(String data) {
		// WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleLastName, data);
		return this;

	}
	@FindBy(how=How.CLASS_NAME, using = "smallSubmit") WebElement eleCreateLead;
	public HomePage clickCreateLead() {
		click(eleCreateLead);
		return new HomePage();
	}

}
